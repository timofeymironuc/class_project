let balles = 0;
let questions = ["Самый тяжёлый стабильный элемент?", "Какой реактор из перичисленных считается более безопасным?", "Какой изотоп урана нужен для обычных реакторов(на медленных нейтронах)", `Вы набрали`];
let variants_of_answer = [["Торий", "Вольфрам", "Свинец"],["РБМК-1200", "РИТМ_200", "ВВЭР-1000", "БН-800"], ["235", "238"], ["Повторить?"]];
let right_answers = [2, 2, 0, 0];

let number = -1;

function create_buttons(){
    for(let i = 0; i < variants_of_answer[number].length; i++){
        $(".answers").append(`<button type = "button" class = "btn btn-primary anwswers_buttom col-5" onclick = "buttom_answer(${i})">${variants_of_answer[number][i]}</button>`);
    }
}

function change_icon(adress, icon, height, width){
    $(adress).replaceWith(icon);
    $(adress).attr("width", width);
    $(adress).attr("height", height);
}

function buttom_answer(button){
    if(button == right_answers[number]){
        if(number == questions.length-1){
            number = -1;
        }
        else{
            console.log(balles);
            balles++;
        }
        change_icon(".victor > svg",
        '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check" viewBox="0 0 16 16"><path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"/></svg>',
        "50", "50");
    }

    else{
        change_icon(".victor > svg",
        '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16"><path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/></svg>',
        "50", "50");
    }

    setTimeout(change_icon, 1000, ".victor > svg",
    '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-question-lg" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M4.475 5.458c-.284 0-.514-.237-.47-.517C4.28 3.24 5.576 2 7.825 2c2.25 0 3.767 1.36 3.767 3.215 0 1.344-.665 2.288-1.79 2.973-1.1.659-1.414 1.118-1.414 2.01v.03a.5.5 0 0 1-.5.5h-.77a.5.5 0 0 1-.5-.495l-.003-.2c-.043-1.221.477-2.001 1.645-2.712 1.03-.632 1.397-1.135 1.397-2.028 0-.979-.758-1.698-1.926-1.698-1.009 0-1.71.529-1.938 1.402-.066.254-.278.461-.54.461h-.777ZM7.496 14c.622 0 1.095-.474 1.095-1.09 0-.618-.473-1.092-1.095-1.092-.606 0-1.087.474-1.087 1.091S6.89 14 7.496 14Z"/></svg>',
    "50", "50");

    setTimeout(() => $(".answers >").detach(), 1000);
    console.log(number);
    setTimeout(create_buttons, 1000);
    
    setTimeout(() => $(".text_question").text(questions[number]), 1000);

    if(number == questions.length-2){
        questions[3] = `${questions[3]} ${String(balles)} балла`;
    }

    number++;
}
